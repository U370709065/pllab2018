use strict; #prevent fatal errors. forces to declare scope of each varialbe
use warnings; #if we have errors its giving error messages. makes things easier

my $scalarVariable = 5;

print 'The value of $scalarVariable is ' . "$scalarVariable" . "\n"; 

$scalarVariable = "this is a string";

print 'The value of $scalarVariable is ' . "$scalarVariable" . "\n";

my @a = (10,20,30);

@a = (10, "this is a string", 5.9, $scalarVariable);

print "@a\n";
