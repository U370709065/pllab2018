use strict;
use warnings;

my %grades = ("Ahmet"=>80, "Zeynep"=>50, "Kemal"=>100);

print "sort by key - alphabetical order \n";
foreach my $student (sort keys %grades){
    print "$student $grades{$student}\n";

}


print "sort by value-num order\n";
foreach my $student (sort {$grades{b} <=> $grades{a}} keys %grades){
    print "$student $grades{$student}\n";
}


my @a = ([1,2,3],[4,5,6],[7,8,9]);

print "$a[2][1] $a[1][2]\n";

my $row = scalar @a;
my $cols = scalar @{$a[0]};

for(my $i=0; $i<$row; $i++){
    for(my $j=0; $j<$cols; $j++){
        print "$a[$j][$i] ";
    }
    print "\n";
}


#array of hashes

my @aoh = ({husband=>"barney" , wife=>"betty" , son=>"bab bom"},
           {husband=>"george" , wife=>"jane" , son=>"elroy"},
           {husband=>"homer" , wife=>"marge" , son=>"bart"});


my $hash = %{$aoh[1]};

print ${$aoh[1]} {wife};


foreach my $element (@aoh) {
   my %tmpHash = %{$element};
   foreach my $key (sort keys %tmpHash){ #bütün keyleri array olarak döndürüyor
        print "$key: $tmpHash{$key}\t";
   } 
   print("\n");
}







