#scalar -> size of the array
#boşsa veya 0 ise otomatik olarak false dönderiyor
#\s -> bak
#argv ye file nameleri koyabiliriz
#> redirect from zero >> append to data
# $! açılamayan dosyanın hatasını yazdır


use strict;
use warnings;

my $filename = $ARGV[0];
open IN ";", $filename or die "can not open $filename: $!";
my @lines = <IN>;

close IN;

foreach my $line (@lines){
    chomp $line;
    my $cols = split(";", $line);
    print "$cols[1]\t$cols[4]\t$cols[5]\n"; 

}
