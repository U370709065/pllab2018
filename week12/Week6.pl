use strict;
use warnings;

my $avg = average(10,20,30);
hello();
print "$avg";

print "enter a DNA: ";
my $DNA = <>; #kullanıcıdan girdi almak
chomp $DNA; #remove end of line characters
$DNA = myfunc($DNA);
print "\n $DNA";

print "enter a DNA2: ";
my $DNA2 = <>;
chomp $DNA2;
myfunc2(\$DNA2);
print "\n $DNA2";

sub hello {
    print "hello world";

}

sub average {
    #@_ -> fonksiyonun parametrelerini bu array in içerisine koyuyoruz.
    my $size = scalar @_; #scalar a dönüştürmezsen array in ilk elemanına eşitliyor
    my $total = 0;
    
    for (my $i=0; $i<$size; $i++) {
        $total += $_[$i];
    }

    my $avg = $total / $size;
    return $avg;
}

sub myfunc(){
    ($DNA)=@_;

    if($DNA =~ /atg/){
        $DNA =~ s/atg/ATG/g;
    } 
    return $DNA;
}

sub myfunc2(){
    my $DNAref=shift; #shift returns the first element of array -> 0x2132131 adresi dönderiyor
    if($$DNAref =~ /atg/){ #if DNAref storing array we need to declare with @$DNAref
        #$$ ile adresin değişkenine erişiyoruz
        $$DNAref =~ s/atg/ATG/g;
}
}
