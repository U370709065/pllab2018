use strict;
use warnings;

#my @array = qw/a lsit of words/;
#my $a = "a string";

my @arr = ("ali", "mehmet", "zeynep");

my $size = @arr; # scalar = array;

($a) = @arr; #list context

print $a, "\n"; # ilk elementi yazdiriyor

my ($a, $b, $c, $d) = @arr;

print $size, "\n";


# . -> javada + gibi string concatination a denk geliyor. ' ' tırnak içinde kullanmak gerekiyor. "" da olmuyor.
# "" tırnak içerisinde iki tane değişken yazarsak interpolation olmuş oluyor.

print "@arr\n";

#ikisidie aynı şeyi yapıyor. quoat word boşluklara göre ayırıp array yapiyor.
#my @gene_array = ('EGF', 'TFC', 'CTF');
#my @gene_array = qw/EGF TCF CTF/; 

my @arr2 = qw/This is just a sentence/;

print "$arr2[2]\n";



