#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <stdbool.h>

int main ( void ){

bool b;
char c;
short s;
int i;
double r;
float f;
int x = 10000000, y = 100000000;

printf("The size of bool is:%ld\n",sizeof(b));
printf("The size of short is:%ld\n",sizeof(s));
printf("The size of int is:%ld\n",sizeof(i));
printf("The size of char is:%ld\n",sizeof(c));
printf("The size of double is:%ld\n",sizeof(r));
printf("The size of float is:%lu\n", sizeof(f));
printf("%d\n", x*y);

return 0;
}

